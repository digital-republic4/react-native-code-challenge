
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import HomeScreen from './src/pages/home'
import ParedeScreen from './src/pages/paredes'
import ViewSalaScreen from './src/pages/view'

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
          <Stack.Screen name='Home' component={HomeScreen}/>
          <Stack.Screen name='Paredes' component={ParedeScreen}/>
          <Stack.Screen name='ViewSala' component={ViewSalaScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

