import axios from 'axios';
import {BASE_URL} from "@env";


const instace = axios.create({
    baseURL: BASE_URL
})

export default instace