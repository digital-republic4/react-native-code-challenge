import react from "react";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        marginTop:10,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    item:{
        width: '80%',
        marginVertical: 20
    }
})

export default styles;