import react, { useEffect, useState } from "react";
import {BASE_URL} from "@env";

import {  Text, Button, TouchableOpacity, View, ScrollView } from "react-native";
import styles from './styles'
import api from '../../services/api'

export default function HomeScreen ({navigation}) {
    const [sala, setSala] = useState([]);
    useEffect(() => {
        const salas = async () =>{
            const resposta = await api.get(BASE_URL).then((response) => {
                console.log(response.data)
                return response.data
            })
            setSala(resposta)
            
            
        }
        salas()
    },[])
    
    
    return(
        <View style={styles.container}>
            
                <Button title="Criar Sala" onPress={() => navigation.navigate('Paredes')}>
                    
                </Button>
                <ScrollView>
                    {sala.map((value)=> {
                        return (
                            <View style={styles.item}>
                                <Text>Sala: {value.id}</Text>
                                <Text>metros: {value.metros_total}</Text>
                                <Text>Litros: {value.litros}</Text>
                            </View>
                        )
                    })}
                </ScrollView>
                
        </View>
        
        
    )
}
