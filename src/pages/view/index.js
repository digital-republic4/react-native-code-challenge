import react, { useEffect, useState } from "react";
import {BASE_URL} from "@env";
import {  Text, Button, TouchableOpacity, View } from "react-native";
import styles from './styles'
import api from '../../services/api'

export default function ViewSalaScreen ({route, navigation}) {
    const [litros, setLitros] = useState();
    const {sala_id} = route.params;

    useEffect(() =>{
        const sala = async () => {
            console.log(sala_id)
            const response = await api.get(BASE_URL+'create/'+sala_id).then((response) => {
                console.log(response.data.message)
                return response.data.message
            })
            setLitros(response)
        }
        sala();
        console.log(litros)
        
    },)
    
    
    return(
        <>
        <View style={styles.container}>
            <Text style={styles.h1}>
                Sala {sala_id}
            </Text>
            <Text style={styles.p}>
            {litros}
            </Text>
            
        </View>
        <View style={styles.container}>
            <Button onPress={() => navigation.popToTop()}>Voltar para Home</Button>
        
        </View>
        </>
        
    )
}