import react from "react";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    h1:{
        fontSize:50,
        marginVertical: 12
    },
    p:{
        fontSize:24,
        textAlign: "center"
    }
})

export default styles;