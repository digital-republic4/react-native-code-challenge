import react, { useState } from "react";
import { Text, TextInput, View, Switch, Button } from "react-native";
import api from '../../services/api'
import styles from './styles'
import {BASE_URL} from "@env";

export default function ParedesScreen ({navigation}) {
    const [count, setCount] = useState(1);
    const [altura, setAltura] = useState();
    const [largura, setLargura] = useState();
    const [porta, setPorta] = useState(false);
    const [janela, setJanela] = useState(false);
    const [paredes, setParedes] = useState([]);

    const [isErro, setIsErro] = useState(false);
    const [messageErro, setMessageErro] = useState('');
    const [DisableCriar, setDisableCriar] = useState(false)
    const togglePorta = () => setPorta(previousState => !previousState);
    const toggleJanela = () => setJanela(previousState => !previousState);

    async function enviarParedes (){
         api.post(BASE_URL+"parede/store", paredes).then((response) => {
            if(response.status == 201){
                navigation.navigate('ViewSala', {sala_id: response.data.message})
            }
        })
        
    }
    function verificador() {
        
        var metro = largura * altura;
        var metroFinal = metro;
        console.log(metro)
        if(janela == true){
            var valorJanela = 2.00 * 1.20;
            metroFinal = metroFinal - valorJanela;
        }
        if(janela ==  true && porta == true){
            var valorPorta = 0.80 * 1.90;
            var portaJanela = valorJanela + valorPorta;
        }
        if(porta == true){
            var valorPorta = 0.80 * 1.90;
            metroFinal = metroFinal - valorPorta  
                if(altura - 1.90 <= 0.30){
                    setIsErro(true)
                    setMessageErro("A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta")
                }
        }
        
        if(metro >= 1 && metro <= 50 && isErro == false){
            console.log(largura)
            if(metro * 0.5 < valorPorta || metro * 0.5 < valorJanela || metro * 0.5 < portaJanela){
                setIsErro(true)
                setMessageErro("O total de área das portas e janelas deve ser no máximo 50% da área de parede")
            }else{
               
                    
                    paredes.push({
                        altura: altura,
                        largura: largura,
                        metro_quadrado: metroFinal,
                        porta: porta,
                        janela: janela
                    })
                
                console.log(paredes)
                setAltura('')
                setLargura('')
                setPorta(false)
                setIsErro(false)
                setJanela(false)
                setCount(previousState => previousState + 1)
                if(count == 4){
                    setDisableCriar(true)
                }
                
                
            }
        }
    }

    return(
        <View style={styles.container}>
            <Text style={styles.erro}>{messageErro}</Text>
            <Text>Altura</Text>
            <TextInput style={styles.input} onChangeText={newText => setAltura(newText)} defaultValue={altura}/>
            <Text>Largura</Text>
            <TextInput style={styles.input} onChangeText={newText => setLargura(newText)} defaultValue={largura}/>
            <View style={styles.coluna}>
                <Switch
                onValueChange={togglePorta}
                value={porta}
                />
                <Text>Porta</Text>
            </View>

            <View style={styles.coluna}>
                <Switch
                onValueChange={toggleJanela}
                value={janela}
                />
                <Text>Janela</Text>
            </View>
            <Button  title="Criar Parede" onPress={() => verificador()} disabled={DisableCriar} >

            </Button>
            <View style={styles.container}>
            <Button  title="Salvar paredes" onPress={() => enviarParedes()} disabled={!DisableCriar} >
            </Button>
                <Text>
                    
                </Text>
            </View>
        </View>
        
        
    )
}