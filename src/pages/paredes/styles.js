import react from "react";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginVertical: 20

    },
    input:{
        width: '70%',
        height: 40,
        borderRadius: 15,
        backgroundColor: '#ffffff'
    },
    coluna:{
        flexDirection: 'row',
        alignItems: 'center',
    },
    erro:{
        color: 'red',
        textAlign: "center"
    }
})

export default styles;